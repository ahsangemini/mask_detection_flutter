import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Notifications {
  Notifications._();

  static void showSnackBar(BuildContext context, String message,
      {Widget icon, int duration, Key key}) {
    Flushbar(
      icon: icon ??
          Icon(
            FontAwesomeIcons.exclamationCircle,
            color: Colors.white,
          ),
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
      borderRadius: 8,
      messageText: Text(
        message ?? 'Sorry, an error has ocurred',
        style: TextStyle(color: Colors.white),
      ),
      backgroundGradient: LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xff2ccfed),
          Color(0xff239ab0),
          Color(0xff17616f),
        ],
      ),
      duration: Duration(seconds: duration ?? 2),
    ).show(context);
  }
}
