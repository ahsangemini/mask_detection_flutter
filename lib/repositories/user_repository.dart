import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:mask_detection_flutter/models/user.dart';
import 'package:mask_detection_flutter/utils/euclideanDist.dart';
import 'package:path/path.dart' as path;
import 'package:uuid/uuid.dart';

class UserRepository {
  final FirebaseAuth _firebaseAuth;
  final FirebaseFirestore _firestore;

  UserRepository()
      : _firebaseAuth = FirebaseAuth.instance,
        _firestore = FirebaseFirestore.instance;

/*
* this function we use when the user tries to login, we get the email and 
* password from firebase authentication, if the user doesn't exists it return "false"
*/
  Future<bool> signInWithCredentials(String email, String password) async {
    User user = (await _firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password))
        .user;
    return user != null;
  }

  // storing the data of the user in firebase
  _storeUserEntityAndImage(
    String userId,
    String name,
    String email,
    String userName,
    List<dynamic> vector,
    File rawfile,
  ) async {
    File file = rawfile;
    String _randomFileNameUUID = Uuid().v4();
    String storageUri =
        'testid/$_randomFileNameUUID${path.extension(file.path)}';
    final StorageReference storageReference =
        FirebaseStorage().ref().child(storageUri);
    final StorageUploadTask uploadTask = storageReference.putFile(file);
    StorageTaskSnapshot storageTaskSnapshot;
    StorageTaskSnapshot snapshot = await uploadTask.onComplete.timeout(
        const Duration(seconds: 60),
        onTimeout: () =>
            throw ('Upload could not be completed. Operation timeout'));
    if (snapshot.error == null) {
      storageTaskSnapshot = snapshot;
      (storageTaskSnapshot.ref.getDownloadURL()).then((fileURL) async {
        var _currentUser = UserModel(
            uid: userId,
            name: name,
            email: email,
            userName: userName,
            vector: vector,
            avatarURI: fileURL);
        _firestore
            .collection("users")
            .doc(_currentUser.uid)
            .set(_currentUser.toMap());
      });
    } else {
      //  Notifications.showSnackBar(context, "Sorry there was an error");
      throw ('An error occured while uploading image. Upload error');
    }
  }

  // the implementation of saving new registered user to firebase
  Future<void> signUp({
    String name,
    String email,
    String userName,
    String password,
    File avatar,
    List<dynamic> vector, // automatically calculated by the CNN model
  }) async {
    try {
      // creating a user of firebaseAuth
      User user = (await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      ))
          .user;
      if (user !=
          null) // if the user is not null then store it on firebase storage
        await _storeUserEntityAndImage(
            user.uid, name, email, userName, vector, avatar);
    } catch (e) {
      log(e);
    }
  }

  Future<void> signOut() async {
    return Future.wait([_firebaseAuth.signOut()]);
  }

  bool isSignedIn() {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  Future<UserModel> getUser() async {
    User user = _firebaseAuth.currentUser;
    UserModel _userModel = UserModel.fromSnapshot(
        await _firestore.collection("users").doc(user.uid).get());
    return _userModel;
  }

  // for login as guest
  Future<UserModel> getanonymousUser() async {
    var _currentUser;
    try {
      User user = (await _firebaseAuth.signInAnonymously()).user;
      DocumentSnapshot snapshot =
          await _firestore.collection("users").doc(user.uid).get();
      if (snapshot.exists) {
        return UserModel.fromSnapshot(snapshot);
      } else {
        _currentUser = UserModel(
            uid: user.uid,
            name: "Guest",
            email: "guest@guest.com",
            userName: "Guest",
            vector: List.generate(192, (index) => index * 0.001),
            avatarURI: "https://picsum.photos/seed/picsum/200/300");
        await _firestore
            .collection("users")
            .doc(_currentUser.uid)
            .set(_currentUser.toMap());
      }
    } catch (e) {
      log(e.toString());
    }
    return _currentUser;
    // User user = (await _firebaseAuth.signInAnonymously()).user;
    // DocumentSnapshot snapshot =
    //     await _firestore.collection("users").doc(user.uid).get();
    // if (snapshot.exists) {
    //   return UserModel.fromSnapshot(snapshot);
    // } else {
    //   var _currentUser = UserModel(
    //       uid: user.uid,
    //       name: "Guest",
    //       email: "guest@guest.com",
    //       userName: "Guest",
    //       vector: [],
    //       avatarURI: "https://picsum.photos/seed/picsum/200/300");
    //   await _firestore
    //       .collection("users")
    //       .doc(_currentUser.uid)
    //       .set(_currentUser.toMap());
    //   return _currentUser;
  }

  Future<List<UserModel>> getAllUsers() async {
    return (await _firestore.collection("users").get())
        .docs
        .map((doc) => UserModel.fromSnapshot(doc))
        .toList();
  }

  Future similarVectorExists(List<dynamic> currentVector) async {
    UserModel similarUser;
    try {
      List<UserModel> allUsers = await getAllUsers();
      similarUser = EuclideanDistance.instance.compare(currentVector, allUsers);
    } catch (e) {
      log(e.toString());
    }
    if (similarUser != null)
      return similarUser;
    else
      return null;
  }
}
