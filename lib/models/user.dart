import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

// user model
class UserModel extends Equatable {
  final String uid;
  final String userName;
  final String name;
  final String email;
  final String avatarURI;
  final List<dynamic> vector;

  UserModel(
      {this.uid,
      this.userName,
      this.name,
      this.email,
      this.avatarURI,
      this.vector});

  // this function uses to get data from firebase
  factory UserModel.fromSnapshot(DocumentSnapshot snapshot) {
    var data = snapshot.data();
    return UserModel(
      uid: snapshot.id,
      name: data['name'],
      userName: data["userName"],
      email: data['email'],
      avatarURI: data['avatarURI'],
      vector: List<dynamic>.from(data['vector']) ?? [],
    );
  }

  // this funtion uses when we map data to firebase
  Map<String, dynamic> toMap() {
    return {
      "name": this.name,
      "email": this.email,
      "userName": this.userName,
      "avatarURI": this.avatarURI,
      "vector": this.vector,
    };
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        name,
        userName,
        email,
        avatarURI,
        vector,
      ];
}
