import 'package:equatable/equatable.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

// handy class to ease the work
class FaceModel extends Equatable {
  final Face face;
  final String label;
  final double confidence;
  final String email;

  FaceModel({
    this.face,
    this.label,
    this.confidence,
    this.email,
  });

  FaceModel copyWith({
    Face face,
    String label,
    double confidence,
    String email,
  }) {
    return FaceModel(
      face: face ?? this.face,
      label: label ?? this.label,
      confidence: confidence ?? this.confidence,
      email: email ?? this.email,
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        face,
        label,
        confidence,
        email,
      ];
}
