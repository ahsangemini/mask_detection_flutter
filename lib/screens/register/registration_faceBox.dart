import 'dart:ui';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// this method has been used to print the input photo from the user
class RegistrationFacePainter extends CustomPainter {
  RegistrationFacePainter(this.imageSize, this.results);
  final Size imageSize;
  double scaleX, scaleY;
  List<Face> results;
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 3.0
      ..color = Colors.greenAccent;
    for (Face face in results) {
      scaleX = size.width / imageSize.width;
      scaleY = size.height / imageSize.height;
      canvas.drawRRect(
          _scaleRect(
              rect: face.boundingBox,
              imageSize: imageSize,
              widgetSize: size,
              scaleX: scaleX,
              scaleY: scaleY),
          paint);
    }
  }

  @override
  bool shouldRepaint(RegistrationFacePainter oldDelegate) {
    return oldDelegate.imageSize != imageSize || oldDelegate.results != results;
  }
}

RRect _scaleRect(
    {@required Rect rect,
    @required Size imageSize,
    @required Size widgetSize,
    double scaleX,
    double scaleY}) {
  return RRect.fromLTRBR(
      (widgetSize.width - rect.left.toDouble() * scaleX),
      rect.top.toDouble() * scaleY,
      widgetSize.width - rect.right.toDouble() * scaleX,
      rect.bottom.toDouble() * scaleY,
      Radius.circular(10));
}
