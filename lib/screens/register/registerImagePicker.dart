import 'dart:developer';
import 'dart:io';
import 'package:mask_detection_flutter/screens/register/registration_faceBox.dart';
import 'package:mask_detection_flutter/utils/detectionUtilities.dart';
import 'package:path_provider/path_provider.dart';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as imglib;
import 'package:tflite_flutter/tflite_flutter.dart' as tfl;
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';

class RegistrationPhoto extends StatefulWidget {
  @override
  RegistrationPhotoState createState() => RegistrationPhotoState();
}

class RegistrationPhotoState extends State<RegistrationPhoto> {
  CameraController _camera;
  tfl.Interpreter interpreter;
  bool _isDetecting = false;
  CameraLensDirection _direction = CameraLensDirection.front;
  double threshold = 1.0;
  List e1;
  bool _faceFound = false;
  bool _multiPleFaces = false;
  List<dynamic> res;
  bool _buttonEnabled = true;
  List<Face> _faces = [];
  Uuid uuid = Uuid();

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    try {
      log("Start");
      _initializeCamera();
      log("End");
    } catch (e) {
      log(e.toString());
    }
  }

  Future loadModel() async {
    try {
      final gpuDelegateV2 = tfl.GpuDelegateV2(
          options: tfl.GpuDelegateOptionsV2(
        false,
        tfl.TfLiteGpuInferenceUsage.fastSingleAnswer,
        tfl.TfLiteGpuInferencePriority.minLatency,
        tfl.TfLiteGpuInferencePriority.auto,
        tfl.TfLiteGpuInferencePriority.auto,
      ));

      // we used tflite interpreter,
      var interpreterOptions = tfl.InterpreterOptions()
        ..addDelegate(gpuDelegateV2);
      // loading the tflite model
      interpreter = await tfl.Interpreter.fromAsset('mobilefacenet.tflite',
          options: interpreterOptions);
    } on Exception {
      print('Failed to load model.');
    }
  }

  void _initializeCamera() async {
    try {
      await loadModel();
      CameraDescription description = await getCamera(_direction);

      _camera = CameraController(description, ResolutionPreset.low,
          enableAudio: false);
      await _camera.initialize();
      log("The camera is initializeing");
      setState(() {});
      _camera.startImageStream((CameraImage image) {
        // the detector starts from here
        if (_camera != null) {
          setState(() {});
          if (_isDetecting)
            return; // if it's already detecting then it should be done (return) and move forward
          _isDetecting = true; // otherwise, we start detecting
          detect(
                  // here we detect methods, its simply the firebase that we give the
                  // fases we have been made, the detect is calculating the faces
                  image: image,
                  detectInImage: _getDetectionMethod(),
                  imageRotation: description.sensorOrientation)
              .then(
            (dynamic result) async {
              setState(() {
                _faces = result;
              });
              if (result.length == 0) {
                // if the length of the photo is zero, so we set the fields of
                // _faceFound and _multiPleFacesto false
                _faceFound = false;
                _multiPleFaces = false;
              } else if (result.length > 1) {
                // is the length is more than one, then it has been detected more than one face
                _multiPleFaces = true;
              } else {
                // otherwise one face has been detected
                _faceFound =
                    true; // the button now is available, and the person can take a photo for himself
              }
              if (_faceFound && !_multiPleFaces) {
                Face _face;
                imglib.Image convertedImage =
                    _convertCameraImage(image, _direction);
                for (_face in result) {
                  double x, y, w, h;
                  // after converting the image to RGB we crop the image
                  // we used image libabry to copy and crop the image
                  x = (_face.boundingBox.left);
                  y = (_face.boundingBox.top);
                  w = (_face.boundingBox.width);
                  h = (_face.boundingBox.height);
                  imglib.Image croppedImage = imglib.copyCrop(convertedImage,
                      x.round(), y.round(), w.round(), h.round());
                  croppedImage = imglib.copyResizeCropSquare(
                      croppedImage, 112); // its important to resize the photo,
                  // in that way we get all the hotos with the same size

                  res = _recog(croppedImage); // returning the vectors to "res"
                }
              }
              setState(() {});
              _isDetecting = false;
            },
          ).catchError(
            (_) {
              _isDetecting = false;
            },
          );
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<List<dynamic>> Function(FirebaseVisionImage image)
      _getDetectionMethod() {
    final faceDetector = FirebaseVision.instance.faceDetector(
      FaceDetectorOptions(
        mode: FaceDetectorMode.accurate,
      ),
    );
    return faceDetector.processImage;
  }

  Widget _buildResults() {
    const Text noResultsText = const Text('');
    if (_faces == null || _camera == null || !_camera.value.isInitialized) {
      return noResultsText;
    }
    CustomPainter painter;

    final Size imageSize = Size(
      _camera.value.previewSize.height,
      _camera.value.previewSize.width,
    );
    painter = RegistrationFacePainter(imageSize, _faces);
    return CustomPaint(
      painter: painter,
    );
  }

  Widget _buildImage() {
    if (_camera == null || !_camera.value.isInitialized) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return Container(
      constraints: const BoxConstraints.expand(),
      child: _camera == null
          ? const Center(child: null)
          : Stack(
              fit: StackFit.expand,
              children: <Widget>[
                CameraPreview(_camera),
                _buildResults(),
              ],
            ),
    );
  }

  // in this method we deal with taking the photo and save it on firebase
  Future<void> exitScreen() async {
    setState(() {
      _buttonEnabled = false;
    });
    try {
      await _camera.stopImageStream();
      String filePath = await takePicture();
      Map<String, dynamic> result = {
        "vector": res,
        "filePath": filePath,
      };
      Navigator.of(context)
          .pop(result); // after the vector has been calculated,
      //we introduce the image in the register screen
    } catch (e) {
      log(e.toString());
      _buttonEnabled = true;
    }

    setState(() {
      _buttonEnabled = true;
    });
  }

  // taking the picture from the user and store it on firebase storage
  Future<String> takePicture() async {
    if (!_camera.value.isInitialized) {
      log('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/profile_photo';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${uuid.v4()}.jpg';

    if (_camera.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await _camera.takePicture(filePath);
    } on CameraException catch (e) {
      log(e.toString());
      return null;
    }
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Your Photo'),
      ),
      body: _buildImage(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: (_faceFound) ? Colors.blue : Colors.blueGrey,
        child: Icon(Icons.camera),
        onPressed: _buttonEnabled
            ? () {
                try {
                  if (_faceFound) exitScreen();
                } catch (e) {
                  setState(() {
                    _buttonEnabled = true;
                  });
                }
              }
            : null,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

/*
 * tha camera image that flutter uses is YUV image format, 
 * implementeing the detecting is not possible with YUV image, so the image have 
 * to be converted to RGB format. 
 */
  imglib.Image _convertCameraImage(
      CameraImage image, CameraLensDirection _dir) {
    int width = image.width;
    int height = image.height;
    // imglib -> Image package from https://pub.dartlang.org/packages/image
    var img = imglib.Image(width, height); // Create Image buffer
    const int hexFF = 0xFF000000;
    final int uvyButtonStride = image.planes[1].bytesPerRow;
    final int uvPixelStride = image.planes[1].bytesPerPixel;
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        final int uvIndex =
            uvPixelStride * (x / 2).floor() + uvyButtonStride * (y / 2).floor();
        final int index = y * width + x;
        final yp = image.planes[0].bytes[index];
        final up = image.planes[1].bytes[uvIndex];
        final vp = image.planes[2].bytes[uvIndex];
        // Calculate pixel color
        int r = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255);
        int g = (yp - up * 46549 / 131072 + 44 - vp * 93604 / 131072 + 91)
            .round()
            .clamp(0, 255);
        int b = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255);
        // color: 0x FF  FF  FF  FF
        //           A   B   G   R
        img.data[index] = hexFF | (b << 16) | (g << 8) | r;
      }
    }
    var img1 = (_dir == CameraLensDirection.front)
        ? imglib.copyRotate(img, -90)
        : imglib.copyRotate(img, 90);
    return img1;
  }

  // this method gives the vectors (that we have to save them in firebase)
  List<dynamic> _recog(imglib.Image img) {
    var byteInput = imageToByteListFloat32(
        img, 112, 128, 128); // moving from binary image to float 32
    List input = byteInput.asFloat32List();
    input = input.reshape([1, 112, 112, 3]); // reshaping the vectors
    List output = List(1 * 192).reshape([1, 192]);
    interpreter.run(input, output);
    output = output.reshape([192]);
    e1 = List.from(output);
    return e1;
  }

  @override
  void dispose() {
    super.dispose();
    if (_camera != null) _camera?.dispose();
    if (interpreter != null) interpreter.close();
  }
}
