import 'dart:developer';
import 'dart:io';

import 'package:mask_detection_flutter/blocs/authentication_bloc/authentication_bloc.dart';
import 'package:mask_detection_flutter/blocs/authentication_bloc/authentication_event.dart';
import 'package:mask_detection_flutter/blocs/register_bloc/register_bloc.dart';
import 'package:mask_detection_flutter/blocs/register_bloc/register_event.dart';
import 'package:mask_detection_flutter/blocs/register_bloc/register_state.dart';
import 'package:mask_detection_flutter/screens/register/registerImagePicker.dart';
import 'package:mask_detection_flutter/widgets/gradient_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_detection_flutter/widgets/notifications.dart';
import 'package:permission_handler/permission_handler.dart';

/*
 * in the registration we save: name, usename email, password and a photo
 * the photo is like a fingerprint, we save vetors of the photo, 
 * we use the vectors for the face detection 
 * 
 * when we do image seaech engine, we compare the photos by this vector, 
 * we calculate the distance between two photos by these vectors
 * 
 * also when the same person when he is already registered, he can't do a
 * registeration one more time, so when the user tries to register for the second
 * time, we give him a messages that he is already registered.
 */

class RegisterForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<RegisterForm> {
  final GlobalKey<FormState> formKey = GlobalKey();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  File _image;
  List<dynamic> _vector = [];
  bool _buttonEnabled = true;
  bool get isPopulated =>
      _emailController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty &&
      _nameController.text.isNotEmpty &&
      _userNameController.text.isNotEmpty &&
      _image != null;

  bool isButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated;
  }

  RegisterBloc _registerBloc;

  @override
  void initState() {
    super.initState();
    _registerBloc = context.bloc<RegisterBloc>();
    _emailController.addListener(_onEmailChange);
    _passwordController.addListener(_onPasswordChange);
  }

  @override
  Widget build(BuildContext context) {
    // "BlocConsumer" gives us the both: listner and builder
    return BlocConsumer<RegisterBloc, RegisterState>(
      buildWhen: (currentState, nextState) => currentState != nextState,
      listener: (context, state) {
        // the listner is a bloc bar as execution
        if (state.isSubmitting) {
          Notifications.showSnackBar(
            context,
            "Registering...",
            duration: 4,
          );
        } else if (state.isSuccess) {
          Navigator.of(context).pop();
          context.bloc<AuthenticationBloc>().add(
                AuthenticationLoggedIn(),
              );
          // checking validation
        } else if (state.isVectorValid == false) {
          Notifications.showSnackBar(
              context, "Your photo already exists in database");
        } else if (state.isNameValid == false) {
          Notifications.showSnackBar(context, "A valid name is required");
        } else if (state.isUserNameValid == false) {
          Notifications.showSnackBar(context, "A valid user name is required");
        } else if (state.isAvatarProvided == false) {
          Notifications.showSnackBar(
              context, "A valid Profile Photo is Required");
        }
      },
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _nameController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: "Name",
                  ),
                  keyboardType: TextInputType.name,
                  autovalidate: true,
                  autocorrect: false,
                ),
                TextFormField(
                  controller: _userNameController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.email),
                    labelText: "Username",
                  ),
                  keyboardType: TextInputType.name,
                  autovalidate: true,
                  autocorrect: false,
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.email),
                    labelText: "Email",
                  ),
                  keyboardType: TextInputType.emailAddress,
                  autovalidate: true,
                  autocorrect: false,
                  validator: (_) {
                    return !state.isEmailValid ? 'Invalid Email' : null;
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock),
                    labelText: "Password",
                  ),
                  obscureText: true,
                  autovalidate: true,
                  autocorrect: false,
                  validator: (_) {
                    return !state.isPasswordValid ? 'Invalid Password' : null;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                getImageInput(),
                SizedBox(
                  height: 30,
                ),
                GradientButton(
                  width: 150,
                  height: 45,
                  onPressed: () {
                    // if (state.isVectorValid == false) {
                    //   Notifications.showSnackBar(context,
                    //       "User with similar photo already exists in the database");
                    // }
                    if (state.isVectorValid == false) {
                      Notifications.showSnackBar(
                          context, "Your photo already exists in database");
                    }
                    if (isButtonEnabled(state)) {
                      _onFormSubmitted();
                    }
                    log(state.isVectorValid.toString());
                  },
                  text: Text(
                    'Register',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  icon: Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  // this is the widget of the getting tho photo from the user in the registeration
  Widget getImageInput() => GestureDetector(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.zero,
            clipBehavior: Clip.antiAlias,
            elevation: 16,
            color: _image != null ? Colors.grey[700] : Colors.white,
            child: Opacity(
                opacity: 1,
                child: _image != null
                    ? Image.file(
                        _image,
                        fit: BoxFit.cover,
                      )
                    : Container(
                        height: 60,
                        width: 60,
                        child: Icon(
                          Icons.add_photo_alternate,
                          color: Colors.black87,
                          size: 35,
                        ),
                      )),
          ),
        ],
      ),
      onTap: () {
        try {
          if (_buttonEnabled) getProfilePhoto();
        } catch (e) {
          _buttonEnabled = true;
          log(e.toString());
        }
      });

  Future<void> onTapGetPhoto() async {
    await getProfilePhoto();
  }

  // this function returns the image with the vectors.
  Future<void> getProfilePhoto() async {
    _buttonEnabled = false;
    if (await Permission.camera.request().isGranted) {
      Map result = {};
      try {
        result = await Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => RegistrationPhoto(),
          ),
        );
        _buttonEnabled = true;
      } catch (e) {
        _buttonEnabled = true;
        log(e.toString());
      }

      if (result.isNotEmpty) {
        setState(() {
          _image = File(result['filePath']); // the result stored here
          _vector = result['vector']; // the vector stored here
        });
      }
    }
  }

  // if the user inserts non valid email, he has to reenter a valid email
  void _onEmailChange() {
    _registerBloc.add(RegisterEmailChanged(email: _emailController.text));
  }

  // if the user inserts non valid email, he has to reenter a valid email
  void _onPasswordChange() {
    _registerBloc
        .add(RegisterPasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _registerBloc.add(RegisterSubmitted(
      name: _nameController.text,
      userName: _userNameController.text,
      avatarFile: _image,
      vector: _vector,
      email: _emailController.text,
      password: _passwordController.text,
    ));
  }
}
