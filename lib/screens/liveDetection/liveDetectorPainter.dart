import 'dart:ui';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mask_detection_flutter/models/faceModel.dart';

class FaceDetectorPainter extends CustomPainter {
  FaceDetectorPainter(this.imageSize, this.results);
  final Size imageSize;
  double scaleX, scaleY;
  List<FaceModel> results;
  Face face;
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 3.0;

    for (FaceModel facemodel in results) {
      // face = results[label];
      scaleX = size.width / imageSize.width;
      scaleY = size.height / imageSize.height;
      canvas.drawRRect(
          _scaleRect(
              rect: facemodel.face.boundingBox,
              imageSize: imageSize,
              widgetSize: size,
              scaleX: scaleX,
              scaleY: scaleY),
          facemodel.label == "WithoutMask"
              ? facemodel.email == null
                  ? (paint..color = Colors.yellow)
                  : (paint..color = Colors.red)
              : (paint..color = Colors.blue));
      TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
      textPainter.text = TextSpan(
        children: [
          TextSpan(
            text: "${facemodel?.email ?? " "}\n",
            style: TextStyle(
              fontSize: 12.0,
              color: facemodel.label == "WithoutMask"
                  ? facemodel.email == null ? (Colors.yellow) : (Colors.red)
                  : (Colors.blue),
              fontWeight: FontWeight.normal,
            ),
          ),
          TextSpan(
            text: "Confidence: ${facemodel.confidence.toStringAsFixed(2)}",
            style: TextStyle(
              fontSize: 12.0,
              color: facemodel.label == "WithoutMask"
                  ? facemodel.email == null ? (Colors.yellow) : (Colors.red)
                  : (Colors.blue),
              fontWeight: FontWeight.normal,
            ),
          )
        ],
      );

      textPainter.layout();
      textPainter.paint(
          canvas,
          Offset(facemodel.face.boundingBox.topLeft.dx + 20,
              facemodel.face.boundingBox.topLeft.dy + 20));
    }
  }

  @override
  bool shouldRepaint(FaceDetectorPainter oldDelegate) {
    return oldDelegate.imageSize != imageSize || oldDelegate.results != results;
  }
}

RRect _scaleRect(
    {@required Rect rect,
    @required Size imageSize,
    @required Size widgetSize,
    double scaleX,
    double scaleY}) {
  return RRect.fromLTRBR(
      (widgetSize.width - rect.left.toDouble() * scaleX),
      rect.top.toDouble() * scaleY,
      widgetSize.width - rect.right.toDouble() * scaleX,
      rect.bottom.toDouble() * scaleY,
      Radius.circular(10));
}
