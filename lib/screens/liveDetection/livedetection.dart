import 'dart:developer';
import 'dart:io';
import 'package:mask_detection_flutter/widgets/notifications.dart';
import 'package:mask_detection_flutter/models/faceModel.dart';
import 'package:mask_detection_flutter/models/user.dart';
import 'package:mask_detection_flutter/repositories/detector_repository.dart';
import 'package:mask_detection_flutter/screens/liveDetection/liveDetectorPainter.dart';
import 'package:mask_detection_flutter/utils/detectionUtilities.dart';
import 'package:mask_detection_flutter/utils/email.dart';
import 'package:mask_detection_flutter/utils/getAllUsers.dart';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as imglib;
import 'package:tflite/tflite.dart';
import 'package:tflite_flutter/tflite_flutter.dart' as tfl;
import 'package:flutter/services.dart';

class LiveCameraScreen extends StatefulWidget {
  @override
  LiveCameraScreenState createState() => LiveCameraScreenState();
}

class LiveCameraScreenState extends State<LiveCameraScreen> {
  List<FaceModel> _scanResults = [];
  CameraController _camera;
  var interpreter;
  bool _isDetecting = false;
  CameraLensDirection _direction = CameraLensDirection.front;
  dynamic data = {};
  double threshold = 1.0;
  Directory tempDir;
  List e1 = [];
  bool _faceFound = false;
  bool modelLoaded = false;
  bool _emailSent = false;

  final TextEditingController _name = TextEditingController();
  List<FaceModel> _presentUsers = [];
  List<UserModel> _allUsers = [];
  UserModel _userModel;
  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    try {
      _initializeCamera();
    } catch (e) {
      log(e.toString() + "Error during initialization");
    }
  }

  Future loadModel() async {
    try {
      await DetectorRepository.instance.loadMaskModel();
      final gpuDelegateV2 = tfl.GpuDelegateV2(
          options: tfl.GpuDelegateOptionsV2(
        false,
        tfl.TfLiteGpuInferenceUsage.fastSingleAnswer,
        tfl.TfLiteGpuInferencePriority.minLatency,
        tfl.TfLiteGpuInferencePriority.auto,
        tfl.TfLiteGpuInferencePriority.auto,
      ));

      var interpreterOptions = tfl.InterpreterOptions()
        ..addDelegate(gpuDelegateV2);
      interpreter = await tfl.Interpreter.fromAsset('mobilefacenet.tflite',
          options: interpreterOptions);
    } on Exception {
      print('Failed to load model.');
    }
  }

  void _initializeCamera() async {
    // simple code for initializing the camera (in live)
    if (!modelLoaded) await loadModel();
    modelLoaded = true;
    CameraDescription description = await getCamera(_direction);
    _camera =
        CameraController(description, ResolutionPreset.low, enableAudio: false);
    await _camera.initialize();
    setState(() {});
    UserModel res;
    _allUsers = (await getAllUsersMemorized()) ?? null;
    _camera.startImageStream((CameraImage image) {
      if (_camera != null) {
        if (_isDetecting) return;
        _isDetecting = true;
        detect(
                // this method is part from firebase vision example
                image: image,
                detectInImage:
                    _getDetectionMethod(), // gives the list of users that has been detected
                imageRotation: description.sensorOrientation)
            .then(
          (dynamic result) async {
            if (result?.length == 0) {
              // there is NO face has found
              setState(() {
                _scanResults = [];
                _faceFound = false;
                _isDetecting = false;
                res = null;
              });
            } else
              _faceFound = true;
            Face _face;
            imglib.Image convertedImage =
                _convertCameraImage(image, _direction);
            _presentUsers = [];
            for (_face in result) {
              double x, y, w, h;
              x = (_face.boundingBox.left);
              y = (_face.boundingBox.top);
              w = (_face.boundingBox.width);
              h = (_face.boundingBox.height);
              imglib.Image croppedImage = imglib.copyCrop(
                  convertedImage, x.round(), y.round(), w.round(), h.round());
              croppedImage = imglib.copyResizeCropSquare(
                  croppedImage, 112); // 112 because of the model
              List<dynamic> _recognitions = await faceMasked(croppedImage);

              if (_recognitions[0]['label'] == "WithoutMask") {
                res = _recog(croppedImage, _allUsers);
                _presentUsers.add(
                  FaceModel(
                    face: _face,
                    label: _recognitions[0]['label'],
                    confidence: _recognitions[0]['confidence'],
                    email: res?.email,
                  ),
                );
              } else {
                // the user that has been detected wears a mask
                _presentUsers.add(
                  FaceModel(
                    face: _face,
                    label: _recognitions[0]['label'],
                    confidence: _recognitions[0]['confidence'],
                    email: null,
                  ),
                );
              }
            }
            setState(() {
              if (_userModel != res) {
                _userModel = res;
              }
              _scanResults = _presentUsers;
            });

            _isDetecting = false; // reseting _isDetecting for future usage
          },
        ).catchError(
          (_) {
            _isDetecting = false;
          },
        );
      }
    });
  }

  // this method uses firebase plugin
  Future<List<Face>> Function(FirebaseVisionImage image) _getDetectionMethod() {
    final faceDetector = FirebaseVision.instance.faceDetector(
      FaceDetectorOptions(
        mode: FaceDetectorMode.accurate,
      ),
    );
    return faceDetector.processImage;
  }

  Widget _buildResults() {
    const Text noResultsText = const Text('');
    if (_scanResults == null ||
        _camera == null ||
        !_camera.value.isInitialized) {
      return noResultsText;
    }
    CustomPainter painter;

    final Size imageSize = Size(
      _camera.value.previewSize.height,
      _camera.value.previewSize.width,
    );
    painter = FaceDetectorPainter(imageSize, _scanResults);
    return CustomPaint(
      painter: painter,
    );
  }

  Widget _buildImage() {
    if (_camera == null || !_camera.value.isInitialized) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return Container(
      constraints: const BoxConstraints.expand(),
      child: _camera == null
          ? const Center(child: null)
          : Stack(
              fit: StackFit.expand,
              children: <Widget>[
                CameraPreview(_camera),
                _buildResults(),
              ],
            ),
    );
  }

  void _toggleCameraDirection() async {
    if (_direction == CameraLensDirection.back) {
      _direction = CameraLensDirection.front;
    } else {
      _direction = CameraLensDirection.back;
    }
    await _camera.stopImageStream();
    await _camera.dispose();

    setState(() {
      _camera = null;
    });

    _initializeCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mask  Detection Live'),
      ),
      body: _buildImage(),
      floatingActionButton:
          Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        FloatingActionButton(
          backgroundColor: (_userModel != null && !_emailSent)
              ? Colors.blue
              : Colors.blueGrey,
          child: Icon(Icons.mail),
          onPressed: () async {
            if (_userModel != null)
              setState(() {
                if (!_emailSent) {
                  sendEmail(_userModel.email);
                  _emailSent = true;
                } else {
                  Notifications.showSnackBar(
                      context, "The message has already been sent.");
                }
              });
            // try {
            //   await FlutterEmailSender.send(
            //       SendEmail.instance.getEmail(_userModel));
            // } catch (e) {
            //   log(e.toString());
            // }
          },
          heroTag: null,
        ),
        SizedBox(
          height: 10,
        ),
        FloatingActionButton(
          onPressed: _toggleCameraDirection,
          heroTag: null,
          child: _direction == CameraLensDirection.back
              ? const Icon(Icons.camera_front)
              : const Icon(Icons.camera_rear),
        ),
      ]),
    );
  }

  imglib.Image _convertCameraImage(
      CameraImage image, CameraLensDirection _dir) {
    int width = image.width;
    int height = image.height;
    // imglib -> Image package from https://pub.dartlang.org/packages/image
    var img = imglib.Image(width, height); // Create Image buffer
    const int hexFF = 0xFF000000;
    final int uvyButtonStride = image.planes[1].bytesPerRow;
    final int uvPixelStride = image.planes[1].bytesPerPixel;
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        final int uvIndex =
            uvPixelStride * (x / 2).floor() + uvyButtonStride * (y / 2).floor();
        final int index = y * width + x;
        final yp = image.planes[0].bytes[index];
        final up = image.planes[1].bytes[uvIndex];
        final vp = image.planes[2].bytes[uvIndex];
        // Calculate pixel color
        int r = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255);
        int g = (yp - up * 46549 / 131072 + 44 - vp * 93604 / 131072 + 91)
            .round()
            .clamp(0, 255);
        int b = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255);
        // color: 0x FF  FF  FF  FF
        //           A   B   G   R
        img.data[index] = hexFF | (b << 16) | (g << 8) | r;
      }
    }
    var img1 = (_dir == CameraLensDirection.front)
        ? imglib.copyRotate(img, -90)
        : imglib.copyRotate(img, 90);
    return img1;
  }

  UserModel _recog(imglib.Image img, List<UserModel> allUsers) {
    var byteInput = imageToByteListFloat32(img, 112, 128, 128);
    List input = byteInput.asFloat32List();
    input = input.reshape([1, 112, 112, 3]);
    List output = List(1 * 192).reshape([1, 192]);
    interpreter.run(input, output);
    output = output.reshape([192]);
    e1 = List.from(output);
    return compare(
      e1,
      allUsers,
    );
  }

  Future<List<dynamic>> faceMasked(imglib.Image img) async {
    imglib.Image resizedImage = imglib.copyResize(
      img,
      width: 224,
      height: 224,
    );

    List<dynamic> recognitions = await Tflite.runModelOnBinary(
      binary:
          imageToByteListFloat32(resizedImage, 224, 127.5, 127.5).asUint8List(),
      numResults: 3, // defaults to 5
      threshold: 0.1, // defaults to 0.1
      asynch: true,
    );
    return recognitions;
  }

  UserModel compare(List currEmb, List<UserModel> allUsers) {
    if (allUsers.length == 0) return null;
    double minDist = 999;
    double currDist = 0.0;
    UserModel predRes;
    for (UserModel user in allUsers) {
      currDist = euclideanDistance(user.vector, currEmb);
      if (currDist <= threshold && currDist < minDist) {
        minDist = currDist;
        predRes = user;
      }
    }
    print(minDist.toString() + " " + predRes.toString());
    return predRes;
  }

  Future<void> stopCamera() async {
    await _camera.stopImageStream();
    await _camera.dispose();
    await Tflite.close();
    await interpreter.close();
  }

  @override
  void dispose() {
    super.dispose();
    try {
      stopCamera();
    } catch (e) {
      log(e.toString());
    }
  }
}
