import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:mask_detection_flutter/models/user.dart';
import 'package:mask_detection_flutter/screens/drawer.dart';

class HomeScreen extends StatelessWidget {
  final UserModel user;

  const HomeScreen({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    log(user.vector.toString());
    return Scaffold(
      drawer: CustomDrawer(
        user: user,
      ),
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: user.email == "guest@guest.com"
                ? Center(
                    child: RichText(
                      text: TextSpan(
                        // style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          TextSpan(
                            text: "\nHello, guest\n",
                            style: TextStyle(
                              color: Colors.black.withOpacity(1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 50,
                            ),
                          ),
                          TextSpan(
                            text: "\n\n\n\n\nWelcome to",
                            style: TextStyle(
                              color: Colors.deepPurple.withOpacity(1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                            ),
                          ),
                          TextSpan(
                            text: "\n\nMask Detection App",
                            style: TextStyle(
                              color: Colors.deepPurple.withOpacity(1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                : Center(
                    child: RichText(
                      text: TextSpan(
                        // style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          TextSpan(
                            text: "\nHello, ${user.name}\n",
                            style: TextStyle(
                              color: Colors.black.withOpacity(1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 50,
                            ),
                          ),
                          TextSpan(
                            text: "\n\n\n\n\nWelcome to",
                            style: TextStyle(
                              color: Colors.deepPurple.withOpacity(1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                            ),
                          ),
                          TextSpan(
                            text: "\n\nMask Detection App",
                            style: TextStyle(
                              color: Colors.deepPurple.withOpacity(1.0),
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
