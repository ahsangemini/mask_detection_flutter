import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_detection_flutter/widgets/notifications.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mask_detection_flutter/models/faceModel.dart';
import 'package:mask_detection_flutter/models/user.dart';
import 'package:mask_detection_flutter/repositories/detector_repository.dart';
import 'package:mask_detection_flutter/screens/detectionInimage.dart/crop.dart';
import 'package:mask_detection_flutter/screens/detectionInimage.dart/faceBoundingBox.dart';
import 'package:mask_detection_flutter/utils/email.dart';
import 'package:mask_detection_flutter/utils/euclideanDist.dart';
import 'package:mask_detection_flutter/utils/getAllUsers.dart';
import 'package:image/image.dart' as imglib;
import 'package:tflite/tflite.dart';
import 'package:tflite_flutter/tflite_flutter.dart' as tfl;

// here we detect photo from gallery
class DetectInPhoto extends StatefulWidget {
  DetectInPhoto();

  @override
  _DetectInPhotoState createState() => _DetectInPhotoState();
}

class _DetectInPhotoState extends State<DetectInPhoto> {
  File _imageFile;
  List<Face> _faces;
  bool isLoading = false;
  ui.Image _image;
  ImagePicker _imagePicker = ImagePicker();
  List<dynamic> _embeddingVector;
  double varcosineSimilarity;
  List<File> croppedFaces = [];
  List<List<double>> embeddingVector;
  List<UserModel> allUsers = [];
  List<UserModel> _presentUsers;
  List<dynamic> _recognitions;
  List<FaceModel> _faceModel = [];
  bool modelLoaded = false;
  tfl.Interpreter interpreter;
  UserModel _userModel;
  bool emailSent = false;

  _getImageAndDetectFaces() async {
    try {
      File imageFile =
          File((await _imagePicker.getImage(source: ImageSource.gallery)).path);
      setState(() {
        isLoading =
            true; // whever we use events(because of Bloc), we need to pass parametres to it
      });
      final FirebaseVisionImage firebaseVisionImage =
          FirebaseVisionImage.fromFile(imageFile);
      final faceDetector = FirebaseVision.instance.faceDetector(
        FaceDetectorOptions(
          mode: FaceDetectorMode.accurate,
          enableContours: true,
          enableLandmarks: true,
        ),
      );
      List<Face> faces = await faceDetector.processImage(firebaseVisionImage);

      if (faces.isNotEmpty) {
        _imageFile = imageFile;
        _faces = faces;
        _loadImage(_imageFile, _faces);
      } else {
        _loadWithoutFaceImage(imageFile);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  Future<List<UserModel>> getPresentUsersInPhoto(
      File file, List<Face> faces) async {
    _faceModel = [];
    List<Face> extractedFaces = faces;
    List<UserModel> _presentUsers = [];
    try {
      await Future.wait(extractedFaces.map((face) async {
        Uint8List croppedface; // we save the cropped faces in this variable
        try {
          croppedface = await crop(face, file);
        } on AssertionError {
          try {
            var decodedImage =
                await decodeImageFromList(file.readAsBytesSync());

            croppedface = await crop(face, file,
                rect: Rect.fromLTWH(
                    // we used this to not get out of the borders of the image
                    // cuz when we calculate something outside the photo it
                    // throws an exception, and causes the app to be stoped
                    // so each time when we want to make any calculation we must
                    // check if we are inside the photo or not
                    math.max(face.boundingBox.left, 0),
                    math.max(face.boundingBox.top, 0),
                    math.max(face.boundingBox.topLeft.dx, 0) +
                                face.boundingBox.width >
                            decodedImage.width
                        ? face.boundingBox.width -
                            (face.boundingBox.topLeft.dx +
                                face.boundingBox.width -
                                decodedImage.width)
                        : face.boundingBox.width - 20,
                    face.boundingBox.height));
          } on PlatformException {}
        } catch (e) {
          log(e.toString() + "Crop error");
        }
        imglib.Image croppedImage = imglib.decodeImage(croppedface);
        //decodeImage is a feture from flutter
        _recognitions =
            await DetectorRepository.instance.faceMasked(croppedImage);
        if (_recognitions[0]['label'] == "WithoutMask") {
          // calculating the person that is in the photo (find his email if exists)
          croppedImage = imglib.copyResizeCropSquare(
              croppedImage, 112); // 112 because of the model

          allUsers = await getAllUsersMemorized();

          _embeddingVector = DetectorRepository.instance.recog(croppedImage,
              interpreter); // calculating the vector ftom the interpreter

          _userModel = EuclideanDistance.instance.compare(
            _embeddingVector,
            allUsers,
          );
          setState(() {
            _faceModel.add(
              // adding the model (blue, yellow or red rectangle with the data on them) to the face model
              FaceModel(
                face: face,
                label: _recognitions[0]['label'],
                confidence: _recognitions[0]['confidence'],
                email: _userModel?.email,
              ),
            );
          });
        } else {
          setState(() {
            _faceModel.add(
              FaceModel(
                face: face,
                label: _recognitions[0]['label'],
                confidence: _recognitions[0]['confidence'],
                email: null,
              ),
            );
          });
        }
        if (_userModel != null) _presentUsers.add(_userModel);
      }));
    } catch (e) {
      log(e.toString());
    }
    log("The ending of getPresentUsers in photo");
    return _presentUsers;
  }

  _loadImage(File file, List<Face> faces) async {
    if (!modelLoaded) {
      try {
        modelLoaded = true;
        interpreter = await DetectorRepository.instance.loadModel();
        await DetectorRepository.instance.loadMaskModel();
      } catch (e) {
        log(e.toString());
      }
    }
    croppedFaces = []; // initialize the parameter
    final data = await file.readAsBytes();

    _presentUsers = await getPresentUsersInPhoto(file, faces);
    await decodeImageFromList(data).then(
      (value) => setState(() {
        _image = value;
        isLoading = false;
      }),
    );
  }

  _loadWithoutFaceImage(File file) async {
    final data = await file.readAsBytes();
    varcosineSimilarity = null;
    _faces = [];
    await decodeImageFromList(data).then(
      (value) => setState(() {
        _image = value;
        isLoading = false;
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detect Users In Image"),
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : (_image == null)
              ? Center(child: Text('No image selected'))
              : Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                        "User Presecent in this image are ${_presentUsers?.map((usermodel) => usermodel.email).toString()}"),
                    SizedBox(
                      height: 50,
                    ),
                    Center(
                      child: Container(
                        constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height * 0.7,
                          maxWidth: MediaQuery.of(context).size.width,
                        ),
                        child: FittedBox(
                          child: SizedBox(
                            width: _image.width.toDouble(),
                            height: _image.height.toDouble(),
                            child: CustomPaint(
                              painter: FaceBoundingBox(_image, _faceModel),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FloatingActionButton(
            backgroundColor:
                emailSent ? Colors.blue : Colors.blue.withAlpha(100),
            key: ObjectKey('email'),
            onPressed: () {
              setState(() {
                if (!emailSent) {
                  sendEmail(_presentUsers.last.email);
                  emailSent = true;
                } else {
                  Notifications.showSnackBar(
                      context, "The message has already been sent.");
                }
              });

              // try {
              //   await FlutterEmailSender.send(
              //       SendEmail.instance.getEmail(_presentUsers.last));
              // } catch (e) {
              //   log(e.toString());
              // }
            },
            tooltip: 'Send Email',
            heroTag: null,
            child: Icon(Icons.email),
          ),
          SizedBox(
            height: 20,
          ),
          FloatingActionButton(
            key: ObjectKey('camera'),
            onPressed: _getImageAndDetectFaces,
            tooltip: 'Pick Image',
            child: Icon(Icons.photo),
          ),
        ],
      ),
    );
  }

  Future<void> _dispose() async {
    interpreter.close();
    await Tflite.close();
  }

  @override
  void dispose() {
    super.dispose();
    _dispose();
  }
}
