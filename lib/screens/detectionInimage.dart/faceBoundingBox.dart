import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:mask_detection_flutter/models/faceModel.dart';

/*
 * this class make a box automatically around the face that has been dtected.
 * for every situation we have a specific color:
 * RED = user doesn't use a mask and he is in the database
 * YELLOW = user doesn't wears a mask and he isn't in the database
 * BLUE = user wears a mask
 * then after recognizing which color of the box should be we print the result 
 * on the screen 
 */
class FaceBoundingBox extends CustomPainter {
  final ui.Image image;
  final List<FaceModel> faceModels;
  final List<Rect> rects = [];

  FaceBoundingBox(this.image, this.faceModels) {
    faceModels.forEach((model) {
      rects.add(model.face.boundingBox);
    });
  }

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 15.0;

    canvas.drawImage(image, Offset.zero, Paint());

    faceModels.forEach((model) {
      canvas.drawRect(
          model.face.boundingBox,
          model.label == "WithoutMask"
              ? model.email == null
                  ? (paint..color = Colors.yellow)
                  : (paint..color = Colors.red)
              : (paint..color = Colors.blue));
      TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
      textPainter.text = TextSpan(
        children: [
          TextSpan(
            text: "${model?.email ?? "Not Present"}\n",
            style: TextStyle(
              fontSize: 12.0,
              color: model.label == "WithoutMask"
                  ? model.email == null ? (Colors.yellow) : (Colors.red)
                  : (Colors.blue),
              fontWeight: FontWeight.normal,
            ),
          ),
          TextSpan(
            text: "Confidence: ${model.confidence.toStringAsFixed(2)}",
            style: TextStyle(
              fontSize: 12.0,
              color: model.label == "WithoutMask"
                  ? model.email == null ? (Colors.yellow) : (Colors.red)
                  : (Colors.blue),
              fontWeight: FontWeight.normal,
            ),
          )
        ],
      );

      textPainter.layout();
      textPainter.paint(
          canvas,
          Offset(model.face.boundingBox.topLeft.dx + 20,
              model.face.boundingBox.topLeft.dy + 20));
    });
  }

  @override
  bool shouldRepaint(FaceBoundingBox oldDelegate) {
    return image != oldDelegate.image || faceModels != oldDelegate.faceModels;
  }
}
