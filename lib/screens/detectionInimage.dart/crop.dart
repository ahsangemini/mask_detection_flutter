import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_editor/image_editor.dart';

Future<Uint8List> crop(Face face, File image, {Rect rect}) async {
  //final img = await getImageFromEditorKey(editorKey);
  ImageEditorOption option = ImageEditorOption();
  option.addOption(ClipOption.fromRect(rect ?? face.boundingBox));
  option.addOption(RotateOption(face.headEulerAngleZ.toInt()));
  option.outputFormat = OutputFormat.png(88);
  final result = await ImageEditor.editImage(
    image: image.readAsBytesSync(),
    imageEditorOption: option,
  );
  return result;
}
