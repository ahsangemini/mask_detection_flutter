import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_detection_flutter/blocs/authentication_bloc/authentication_bloc.dart';
import 'package:mask_detection_flutter/blocs/authentication_bloc/authentication_event.dart';
import 'package:mask_detection_flutter/models/user.dart';
import 'package:mask_detection_flutter/screens/detectionInimage.dart/maskDetectionImage.dart';
import 'package:mask_detection_flutter/screens/liveDetection/livedetection.dart';
import 'package:permission_handler/permission_handler.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key key, this.user}) : super(key: key);
  final UserModel user;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(user.name),
            accountEmail: Text(user.email),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(user.avatarURI),
            ),
          ),
          ListTile(
            title: Text("Detect in Photo"),
            onTap: () async {
              bool photos = await Permission.photos.request().isGranted;
              bool medilib = await Permission.mediaLibrary.request().isGranted;
              bool perm = await Permission.storage.request().isGranted;

              if (photos)
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetectInPhoto(),
                    ));
            },
          ),
          ListTile(
            title: Text("Detect Live"),
            onTap: () async {
              bool camera = await Permission.camera.request().isGranted;
              bool microphone = await Permission.microphone.request().isGranted;
              if (camera && microphone)
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LiveCameraScreen()));
            },
          ),
          ListTile(
            title: Text('Logout'),
            onTap: () => BlocProvider.of<AuthenticationBloc>(context)
                .add(AuthenticationLoggedOut()),
          )
        ],
      ),
    );
  }
}
