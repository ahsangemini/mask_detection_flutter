import 'package:intl/intl.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:mailer/mailer.dart';

void sendEmail(String email) async {
  DateFormat formattedDate = DateFormat('EEE d MMM,').add_jm();
  String username = "maskdetection11@gmail.com";
  String password = "lolo123lolo123";

  final smtpServer = gmail(username, password);
  // Use the SmtpServer class to configure an SMTP server:
  // final smtpServer = SmtpServer('smtp.domain.com');
  // See the named arguments of SmtpServer for further configuration
  // options.

  // Create our message.
  final message = Message()
    ..from = Address(username, 'Mask Detection')
    ..recipients.add(email)
    ..subject =
        'Alert From Mask Detection App :: 😀 :: ${formattedDate.format(DateTime.now())}'
    ..text = 'This is the plain text.\nThis is line 2 of the text part.'
    ..html =
        "<h1>ALERT!</h1>\n\n<p>Hey, You have been detected without mask! You owe 20 dollars to the police.</p>"; // body of the email

  try {
    final sendReport = await send(message, smtpServer);
    print('Message sent: ' + sendReport.toString());
  } on MailerException catch (e) {
    print('Message not sent.');
    for (var p in e.problems) {
      print('Problem: ${p.code}: ${p.msg}');
    }
  }
}
