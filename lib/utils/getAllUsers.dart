import 'dart:developer';

import 'package:async/async.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mask_detection_flutter/models/user.dart';

final _closeAllUsers = AsyncMemoizer<List<UserModel>>();

// handy method, stores the users in the firestore
Future<List<UserModel>> getAllUsersMemorized() =>
    _closeAllUsers.runOnce(getAllUsers);

Future<List<UserModel>> getAllUsers() async {
  List<UserModel> _allUsers;
  try {
    List<QueryDocumentSnapshot> documents = (await FirebaseFirestore.instance
        .collection('users')
        .get()
        .then((documents) => documents.docs));
    _allUsers = documents
        .map(
          (doc) => UserModel.fromSnapshot(doc),
        )
        .toList();
    log(documents[0].data()["vector"].toString() + "All Documents");
  } catch (e) {
    log(e.toString() + "Error During Firebase Call");
  }
  return _allUsers;
}
