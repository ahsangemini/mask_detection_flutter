import 'dart:math';
import 'dart:developer' as developer;
import 'package:mask_detection_flutter/models/user.dart';

class EuclideanDistance {
  static final EuclideanDistance instance = EuclideanDistance._internal();

  factory EuclideanDistance() => instance;

  EuclideanDistance._internal();
  double _threshold = 1.0;

  double _euclideanDistance(List e1, List e2) {
    double sum = 0.0;
    for (int i = 0; i < e1.length; i++) {
      sum += pow((e1[i] - e2[i]), 2);
    }
    return sqrt(sum);
  }

  // it gets all the list of users and and compare them with the current vector
  UserModel compare(List<dynamic> currentVector, List<UserModel> allUsers) {
    UserModel predRes;
    try {
      if (allUsers?.length == 0)
        return null; // if no users in the DB then return null

      double minDist = 999;
      double currDist = 0.0;
      predRes = null;

      for (UserModel user in allUsers) {
        currDist = _euclideanDistance(user.vector, currentVector);
        if (currDist <= _threshold && currDist < minDist) {
          minDist = currDist;
          predRes = user;
        }
      }
      developer.log(minDist.toString() + " " + predRes.toString());
    } catch (e) {
      developer.log(e.toString());
    }

    return predRes;
  }
}
