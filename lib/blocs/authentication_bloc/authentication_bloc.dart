import 'package:mask_detection_flutter/blocs/authentication_bloc/authentication_state.dart';
import 'package:mask_detection_flutter/blocs/authentication_bloc/authentication_event.dart';
import 'package:mask_detection_flutter/repositories/user_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'authentication_state.dart';

// this AuthenticationBloc has been provided to the whole app
class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AuthenticationStarted) {
      yield* _mapAuthenticationStartedToState();
    } else if (event is AuthenticationLoggedIn) {
      yield* _mapAuthenticationLoggedInToState();
    } else if (event is AuthenticationLoggedOut) {
      yield* _mapAuthenticationLoggedOutInToState();
    } else if (event is AuthenticationGuestLogin) {
      yield* _mapAuthenticationGuestLoginToState();
    }
  }

  //AuthenticationLoggedOut
  Stream<AuthenticationState> _mapAuthenticationLoggedOutInToState() async* {
    yield AuthenticationFailure();
    _userRepository.signOut();
  }

  //AuthenticationLoggedIn
  Stream<AuthenticationState> _mapAuthenticationLoggedInToState() async* {
    var user = await _userRepository.getUser();
    yield AuthenticationSuccess(user);
  }

  // AuthenticationStarted
  Stream<AuthenticationState> _mapAuthenticationStartedToState() async* {
    final isSignedIn = _userRepository
        .isSignedIn(); // checking firebase authentication (if the user is in the firebase)
    if (isSignedIn) {
      final user = await _userRepository.getUser();
      yield AuthenticationSuccess(user);
    } else {
      yield AuthenticationFailure();
    }
  }

  //Guest Login
  Stream<AuthenticationState> _mapAuthenticationGuestLoginToState() async* {
    try {
      final user = await _userRepository.getanonymousUser();
      yield AuthenticationSuccess(user);
    } catch (e) {
      yield AuthenticationFailure();
    }
  }
}
