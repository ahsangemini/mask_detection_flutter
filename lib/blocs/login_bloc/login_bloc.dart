import 'package:mask_detection_flutter/blocs/login_bloc/login_event.dart';
import 'package:mask_detection_flutter/blocs/login_bloc/login_state.dart';
import 'package:mask_detection_flutter/repositories/user_repository.dart';
import 'package:mask_detection_flutter/utils/validators.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _userRepository;

  LoginBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(LoginState.initial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginEmailChange) {
      yield* _mapLoginEmailChangeToState(event.email);
    } else if (event is LoginPasswordChanged) {
      yield* _mapLoginPasswordChangeToState(event.password);
    } else if (event is LoginWithCredentialsPressed) {
      yield* _mapLoginWithCredentialsPressedToState(
          email: event.email, password: event.password);
    }
  }

  Stream<LoginState> _mapLoginEmailChangeToState(String email) async* {
    yield state.update(isEmailValid: Validators.isValidEmail(email));
  }

  Stream<LoginState> _mapLoginPasswordChangeToState(String password) async* {
    yield state.update(isPasswordValid: Validators.isValidPassword(password));
  }

  Stream<LoginState> _mapLoginWithCredentialsPressedToState(
      {String email, String password}) async* {
    // firebase checkes if the email already exists
    yield LoginState.loading();
    try {
      bool userExists =
          await _userRepository.signInWithCredentials(email, password);
      if (userExists) {
        yield LoginState.success(); // if user exixts we load "success"
      } else {
        yield LoginState
            .failure(); //otherwise (user doesn't exixt) we load "failure"
      }
    } catch (_) {
      yield LoginState.failure();
    }
  }
}
