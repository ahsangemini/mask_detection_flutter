import 'dart:developer';

import 'package:mask_detection_flutter/blocs/register_bloc/register_event.dart';
import 'package:mask_detection_flutter/blocs/register_bloc/register_state.dart';
import 'package:mask_detection_flutter/models/user.dart';
import 'package:mask_detection_flutter/repositories/user_repository.dart';
import 'package:mask_detection_flutter/utils/validators.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// we mapped every event to a state
class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;
  List<UserModel> allUsers;
  RegisterBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(RegisterState.initial());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterEmailChanged) {
      yield* _mapRegisterEmailChangeToState(event.email);
    } else if (event is RegisterPasswordChanged) {
      yield* _mapRegisterPasswordChangeToState(event.password);
    } else if (event is RegisterSubmitted) {
      yield* _mapRegisterSubmittedToState(event: event);
    }
  }

  Stream<RegisterState> _mapRegisterEmailChangeToState(String email) async* {
    yield state.update(isEmailValid: Validators.isValidEmail(email));
  }

  Stream<RegisterState> _mapRegisterPasswordChangeToState(
      String password) async* {
    yield state.update(isPasswordValid: Validators.isValidPassword(password));
  }

  Stream<RegisterState> _mapRegisterSubmittedToState(
      {RegisterSubmitted event, RegisterState state}) async* {
    // checks if the data is valid
    if (event.name == null) {
      yield state.update(isNameValid: false);
    } else if (event.avatarFile == null) {
      yield state.update(isAvatarProvided: false);
    } else if (event.userName == null) {
      yield state.update(isUserNameValid: false);
    } else {
      try {
        // checks if the account already exists
        UserModel user =
            await _userRepository.similarVectorExists(event.vector);
        if (user == null) {
          yield RegisterState.loading();
          await _userRepository.signUp(
            // saving all the data of new user
            name: event.name,
            userName: event.userName,
            email: event.email,
            avatar: event.avatarFile,
            vector: event.vector,
            password: event.password,
          );
          yield RegisterState.success();
        } else {
          yield RegisterState.alreadyExists();
        }
      } catch (error) {
        yield RegisterState.failure();
      }
    }
  }
}
