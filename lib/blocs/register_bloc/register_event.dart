import 'dart:io';

import 'package:camera/camera.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RegisterEmailChanged extends RegisterEvent {
  final String email;

  RegisterEmailChanged({this.email});

  @override
  List<Object> get props => [email];
}

class RegisterPasswordChanged extends RegisterEvent {
  final String password;

  RegisterPasswordChanged({this.password});

  @override
  List<Object> get props => [password];
}

class RegisterNameChanged extends RegisterEvent {
  final String name;

  RegisterNameChanged({this.name});

  @override
  List<Object> get props => [name];
}

class GetAvatarImage extends RegisterEvent {}

class GetUserVectorAndAvatarImage extends RegisterEvent {
  final List<dynamic> cameraImage;
  final File avatarImage;

  GetUserVectorAndAvatarImage(this.cameraImage, this.avatarImage);
}

class RegisterSubmitted extends RegisterEvent {
  final String email;
  final String password;
  final String name;
  final String userName;
  final File avatarFile;
  final List<dynamic> vector;

  RegisterSubmitted(
      {this.name,
      this.userName,
      this.avatarFile,
      this.vector,
      this.email,
      this.password});

  @override
  List<Object> get props =>
      [name, userName, avatarFile, vector, email, password];
}
