import 'package:equatable/equatable.dart';

/*
 * here i saved all states that might happen when the user is regisreting
 * for example: isFormValid, loading, failure, etc...
 */
class RegisterState extends Equatable {
  final bool isNameValid;
  final bool isEmailValid;
  final bool isUserNameValid;
  final bool isAvatarProvided;
  final bool isPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isVectorValid;

  bool get isFormValid =>
      isEmailValid &&
      isPasswordValid &&
      isAvatarProvided &&
      isUserNameValid &&
      isNameValid &&
      isVectorValid;

  RegisterState(
      {this.isNameValid,
      this.isUserNameValid,
      this.isAvatarProvided,
      this.isEmailValid,
      this.isPasswordValid,
      this.isVectorValid,
      this.isSubmitting,
      this.isSuccess,
      this.isFailure});

  factory RegisterState.initial() {
    return RegisterState(
      isEmailValid: true,
      isNameValid: true,
      isUserNameValid: true,
      isAvatarProvided: true,
      isVectorValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory RegisterState.loading() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isNameValid: true,
      isUserNameValid: true,
      isAvatarProvided: true,
      isVectorValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory RegisterState.failure() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isNameValid: true,
      isUserNameValid: true,
      isAvatarProvided: true,
      isVectorValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory RegisterState.alreadyExists() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isNameValid: true,
      isUserNameValid: true,
      isAvatarProvided: true,
      isVectorValid: false,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory RegisterState.success() {
    return RegisterState(
      isEmailValid: true,
      isPasswordValid: true,
      isNameValid: true,
      isUserNameValid: true,
      isAvatarProvided: true,
      isVectorValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
    );
  }

  RegisterState update({
    bool isEmailValid,
    bool isPasswordValid,
    bool isNameValid,
    bool isUserNameValid,
    bool isAvatarProvided,
    bool isVectorValid,
  }) {
    return copyWith(
      isEmailValid: isEmailValid,
      isPasswordValid: isPasswordValid,
      isNameValid: isNameValid,
      isAvatarProvided: isAvatarProvided,
      isUserNameValid: isUserNameValid,
      isVectorValid: isVectorValid,
    );
  }

  RegisterState copyWith({
    bool isEmailValid,
    bool isPasswordValid,
    bool isNameValid,
    bool isUserNameValid,
    bool isAvatarProvided,
    bool isVectorValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
  }) {
    return RegisterState(
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isNameValid: isNameValid ?? this.isNameValid,
      isUserNameValid: isUserNameValid ?? this.isUserNameValid,
      isAvatarProvided: isAvatarProvided ?? this.isAvatarProvided,
      isVectorValid: isVectorValid ?? this.isVectorValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        isEmailValid,
        isPasswordValid,
        isNameValid,
        isUserNameValid,
        isAvatarProvided,
        isVectorValid,
        isSubmitting,
        isSuccess,
        isFailure
      ];
}
